package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.extension.spi.ExtensionRegistry;
import org.asciidoctor.extension.JavaExtensionRegistry;

public class AsciidoctorThemesRegistry implements ExtensionRegistry {

    @Override
    public void register(Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().preprocessor(ThemesPreProcessor.class);
        asciidoctor.javaExtensionRegistry().postprocessor(ThemesPostProcessor.class);
    }
}
