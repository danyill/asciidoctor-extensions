package com.puravida.asciidoctor;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Postprocessor;

import java.io.*;
import java.util.Map;

public class ThemesPostProcessor extends Postprocessor {

    public ThemesPostProcessor(){
    }

    private final String SEARCH = "<link rel=\"stylesheet\" href=\""+ThemesPreProcessor.STYLEDIR+"/";

    @Override
    public String process(Document document, String output) {

        Object obj = document.getAttributes().get("theme");
        if( obj == null ) {
            obj = "asciidoctor";
        }
        String theme = obj.toString();
        int indexOf = output.indexOf(SEARCH);
        if( indexOf != -1 ){
            indexOf += SEARCH.length();
            int endOf = output.indexOf(".",indexOf);

            StringBuffer newOutput = new StringBuffer(output.substring(0,indexOf));
            newOutput.append(theme);
            newOutput.append(output.substring(endOf));
            output = newOutput.toString();

            Object to_dir = document.getOptions().get("to_dir");
            if( to_dir != null ){
                try {
                    to_dir += File.separator+ThemesPreProcessor.STYLEDIR;
                    String css = readResource("/themes/"+theme+".css");
                    File parent = new File(to_dir.toString());
                    parent.mkdirs();
                    File toDir = new File(parent, theme+".css");
                    PrintWriter pw = new PrintWriter(toDir);
                    pw.print(css);
                    pw.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        return output;
    }

    private String readResource(String name) {
        InputStream is = ThemesPostProcessor.class.getResourceAsStream(name);
        if( is == null )
            return "";
        Reader reader = new InputStreamReader( is );
        try {
            StringWriter writer = new StringWriter();
            char[] buffer = new char[8192];
            int read;
            while ((read = reader.read(buffer)) >= 0) {
                writer.write(buffer, 0, read);
            }
            return writer.toString();
        }
        catch (Exception ex) {
            //throw new IllegalStateException("Failed to read '" + name + "'", ex);
            return "";
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                // Continue
            }
        }
    }

}
