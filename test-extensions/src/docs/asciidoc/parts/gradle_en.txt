
[role='language-gb']
=== Gradle

Add PuraVida Bintray repository in your build.gradle and the dependencies

[#repositories,source]
----
repositories {
    maven {
        url  "http://dl.bintray.com/puravida-software/repo"
    }
    // others
}
----

[#dependencies,source,subs="attributes"]
----
dependencies {
    asciidoctor "com.puravida.asciidoctor:asciidoctor-extensions:{extensions-version}"
}
----
