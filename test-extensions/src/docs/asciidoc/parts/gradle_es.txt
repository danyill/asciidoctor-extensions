
[role='language-es']
=== Gradle

Añade el repositorio y las dependencias de PuraVida en Bintray en el fichero build.gradle

[#repositories,source]
----
repositories {
    maven {
        url  "http://dl.bintray.com/puravida-software/repo"
    }
    // others
}
----

[#dependencies,source,subs="attributes"]
----
dependencies {
    asciidoctor "com.puravida.asciidoctor:asciidoctor-extensions:{extensions-version}"
}
----
