
[role='language-gb']
== Google Analytics

If you put the attribute *google-analytics-code* in the header of your document
it will render a javascript code at the end of the HTML with your
Google Analytics code

[#ganalitics,source]
----
= My document
Author
:doctype: book
:google-analytics-code: UA-000000-00
----

....
<!-- google -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-000000-00', 'auto');
ga('send', 'pageview');
</script>
....


.Question
[sidebar]
====
What attribute you must to use if you want to include Google Analytics into your document

quizquestion:ga1en[10] *google-analytics-code*

quizquestion:ga1en[0] *google_analytics_code*

quizquestion:ga1en[0] it's automatically include

====

.Question
[sidebar]
====
How to specify the Google Analytics code

- quizquestion:ga2en[0] editing the fina HTML

- quizquestion:ga2en[10] setting a value to *google-analytics-code*

- quizquestion:ga2en[0] it's include automatically

====

[cols="2s,1a"]
|===

|What attribute is required
|quizzpoints:ga1en[]

|How specify the code
|quizzpoints:ga2en[]

|===

quizztoolbar::[value="Validate"]

