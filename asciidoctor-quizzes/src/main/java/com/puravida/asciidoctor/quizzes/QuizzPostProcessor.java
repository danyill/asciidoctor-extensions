package com.puravida.asciidoctor.quizzes;

import com.puravida.asciidoctor.quizzes.model.Quiz;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Postprocessor;

import java.util.List;
import java.util.Base64;
import java.util.Map;

public class QuizzPostProcessor extends Postprocessor {

    public QuizzPostProcessor(){
    }

    @Override
    public String process(Document document, String output) {
        Quiz quiz = (Quiz)document.getAttributes().get(QuizzPreprocessor.TAG);
        if(quiz == null )
            return output;

        if( quiz.getQuestions().size() == 0)
            return output;

        output = ReadResources.includeJQuery(output);
        output = ReadResources.addJs(output,"/quiz.js");

        String quizString = quiz.toString();
        String solution = Base64.getEncoder().encodeToString(quizString.getBytes());
        output = output.replace("REPLACE_SOLUTION_QUIZZES", solution);

        return output;
    }

}
