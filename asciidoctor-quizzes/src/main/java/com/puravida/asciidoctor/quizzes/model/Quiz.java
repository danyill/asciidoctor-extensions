package com.puravida.asciidoctor.quizzes.model;

import java.util.HashMap;
import java.util.Map;

public class Quiz {


    private Map<String, Questions> questions = new HashMap<String,Questions>();

    public Map<String, Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(Map<String, Questions> questions) {
        this.questions = questions;
    }

    public Question addQuestion(String target, Map<String, Object> attributes){
        if( questions.containsKey(target) == false ){
            questions.put(target, new Questions(target));
        }
        Questions add = questions.get(target);
        return add.addQuestion(attributes);
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{");
        for(Questions question : questions.values()){
            stringBuffer.append(question).append(",");
        }
        stringBuffer.deleteCharAt(stringBuffer.length()-1);
        stringBuffer.append("}");
        return stringBuffer.toString();
    }

}
