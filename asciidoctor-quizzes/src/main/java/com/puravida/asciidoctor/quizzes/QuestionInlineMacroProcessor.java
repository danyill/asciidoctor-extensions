package com.puravida.asciidoctor.quizzes;

import com.puravida.asciidoctor.quizzes.model.Question;
import com.puravida.asciidoctor.quizzes.model.Quiz;
import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.Format;
import org.asciidoctor.extension.FormatType;
import org.asciidoctor.extension.InlineMacroProcessor;
import org.asciidoctor.extension.Name;

import java.util.Map;

@Name("quizquestion")
@Format(value=FormatType.CUSTOM,regexp = "quizquestion:([A-Za-z0-9]+)\\[(.*?)\\]")
public class QuestionInlineMacroProcessor extends InlineMacroProcessor {

    @Override
    public Object process(ContentNode parent, String target, Map<String, Object> attributes) {
        Quiz quiz = (Quiz)parent.getDocument().getAttributes().get(QuizzPreprocessor.TAG);
        if (quiz == null){
            return null;
        }
        return html(quiz.addQuestion( target, attributes));
    }

    String html(Question question){
        return String.format("<input type='%s' id='%s' name='%s' value='' class='quiz-question question-%s'>",
                question.getType(),
                question.getId(),
                question.getName(),
                question.getName());
    }
}
