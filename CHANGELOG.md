# Changelog

Changelog of Puravida Asciidoctor Extension.

## v1.8.0
### No issue

**remove dependency to close a release by the moment**


[ae58aa6f6721608](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/ae58aa6f6721608) jorge *2018-05-28 21:17:46*

**add dependencie to close a release**


[d8b64369425775d](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/d8b64369425775d) jorge *2018-05-28 21:14:17*

**EachFile Processor with extra arguments**


[349503f226c6077](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/349503f226c6077) jorge *2018-05-28 21:05:55*

**typo at each-file example**


[79ade141456a449](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/79ade141456a449) jorge *2018-05-27 10:32:30*

**Change default version strategy to development**


[472ad692fd15ad4](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/472ad692fd15ad4) jorge *2018-05-27 10:21:45*

**Each File PreProcessor**


[3ad72932d2a3bd7](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/3ad72932d2a3bd7) jorge *2018-05-27 10:17:43*

**use git as source for version number**


[9e8a356c72c2d35](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/9e8a356c72c2d35) jorge *2018-05-24 17:59:42*

**update changelog with valid url**


[11ae034a014011a](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/11ae034a014011a) jorge *2018-05-06 19:29:26*

**provide git info to generate valid url in changelog**


[f0b44442a0c35cd](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/f0b44442a0c35cd) jorge *2018-05-06 19:28:30*

**/close**


[443ec886fc79acc](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/443ec886fc79acc) jorge *2018-05-06 16:48:48*

**update docu to show version of extension and themes**


[fc80254f2791886](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/fc80254f2791886) jorge *2018-05-04 13:40:05*


## v1.7.1
### No issue

**Ensure https v1.7.1**


[c618a2122b8a5c3](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/c618a2122b8a5c3) jorge *2018-05-16 23:14:09*


## v1.7-extensions
### No issue

**close v1.7 asciidoctor-extension**


[4ccc1a01b7a4e33](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/4ccc1a01b7a4e33) jorge *2018-05-04 12:57:38*


## v1.7-alpha3
### No issue

**review about dependencies and compileOnly**


[ea4c47c67fc3397](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/ea4c47c67fc3397) jorge *2018-05-04 11:29:04*

**change compile for compileOnly to reduce dependencies**


[e35e781e01018cd](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/e35e781e01018cd) jorge *2018-05-04 07:56:26*


## v1.7-alpha2
### No issue

**remove pdf by the moment**


[4987ddf2f4ac3ab](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/4987ddf2f4ac3ab) jorge *2018-05-02 21:18:47*

**remove pdf by the moment**


[d2749dd3a6ac92c](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/d2749dd3a6ac92c) jorge *2018-05-02 21:02:04*

**Refactoring varios**

 * Intento de usar Tree para pdf pero es incompatible
 * v1.7-alpha2

[52d5eb49023f1b9](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/52d5eb49023f1b9) jorge *2018-05-02 20:53:01*


## v1.7-alpha1
### GitLab 2 

**#2 use  as trigger**


[f3b1f0c2583ffe0](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/f3b1f0c2583ffe0) jorge *2018-04-25 05:07:16*

**#2 use  as trigger**


[091d5565649926a](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/091d5565649926a) jorge *2018-04-25 04:54:09*


### No issue

**Multi language '1.7-alpha1'**


[fc8061b46cc330a](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/fc8061b46cc330a) Jorge Aguilera *2018-04-28 09:05:46*

**release asciidoctor-extensions v1.7-alpha1**


[f20cc52fbdd9606](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/f20cc52fbdd9606) jorge *2018-04-25 04:58:55*

**started version 1.7**


[bca75f04e95703c](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/bca75f04e95703c) jorge *2018-04-25 04:54:30*

**remove test files**


[65a29d562bcbcae](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/65a29d562bcbcae) jorge *2018-04-25 04:38:43*

**update docu theme version**


[6ebbde84e48eee9](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/6ebbde84e48eee9) jorge *2018-04-25 04:36:36*

**v1.1 generate theme only if specified**


[d6bd643f1cbb192](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/d6bd643f1cbb192) jorge *2018-04-16 18:59:50*

**plot 1.0.0-alpha5**


[2ec4b0b9a8a71ca](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/2ec4b0b9a8a71ca) jorge *2018-04-14 10:34:59*

**gnuplot v1**


[576ff1f57c67539](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/576ff1f57c67539) jorge *2018-03-14 07:24:58*

**test run javafx in gitlab-ci**


[812f56bdd449d2b](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/812f56bdd449d2b) jorge *2018-03-07 09:03:02*

**test run javafx in gitlab-ci**


[0c32fc4a84c1301](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/0c32fc4a84c1301) jorge *2018-03-07 08:25:26*

**plot 1.0.0-alpha.2**


[827a06d7b6234cc](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/827a06d7b6234cc) jorge *2018-03-04 22:49:58*

**remove test antora**


[d61e9d66991a893](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/d61e9d66991a893) jorge *2018-03-02 08:24:01*

**test antora**


[ab5e4e2d7ffa98e](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/ab5e4e2d7ffa98e) jorge *2018-03-02 08:21:56*

**plot 1.0.0-alpha.4**


[d635819a11891f4](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/d635819a11891f4) jorge *2018-03-01 05:54:58*

**plot 1.0.0-alpha.2**


[161e95d98239d6c](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/161e95d98239d6c) jorge *2018-03-01 05:12:35*

**improve docu plot**


[b016ded342eb91a](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/b016ded342eb91a) jorge *2018-02-28 22:11:36*

**use plot with alpha.2**


[1975e84a8ca40d9](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/1975e84a8ca40d9) jorge *2018-02-28 22:05:27*

**plot block documented**


[82ccbfac715fd43](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/82ccbfac715fd43) jorge *2018-02-28 12:38:15*

**plot block documented**


[23a98861dda7ee7](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/23a98861dda7ee7) jorge *2018-02-28 12:31:26*

**groogle-chart with plot extension**


[283523336b1a8c1](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/283523336b1a8c1) jorge *2018-02-28 12:24:39*

**test google**


[fb2170d263d8425](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/fb2170d263d8425) jorge *2018-02-27 14:54:28*

**testing test**


[9a2992bbe3cda06](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/9a2992bbe3cda06) jorge *2018-02-27 05:50:09*

**use docker gradle as usual**


[c547bcd3ee8f3fa](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/c547bcd3ee8f3fa) jorge *2018-02-27 05:45:50*

**testing google chart in gitlab**


[bbdbf8b4525d1cd](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/bbdbf8b4525d1cd) jorge *2018-02-27 05:41:58*

**testing google chart in gitlab**


[fba91ee55e84b38](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/fba91ee55e84b38) jorge *2018-02-27 05:31:16*

**testing google chart**


[60d79a7bb94a8d4](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/60d79a7bb94a8d4) jorge *2018-02-27 05:22:24*

**CI plot**


[54b54b6ef309ba7](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/54b54b6ef309ba7) jorge *2018-02-26 06:10:47*

**CI plot**


[ded37a5f3f4332d](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/ded37a5f3f4332d) jorge *2018-02-26 05:57:42*

**CI plot**


[2419eee6679e17c](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/2419eee6679e17c) jorge *2018-02-26 05:53:08*

**CI plot**


[41ecde9377eb30f](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/41ecde9377eb30f) jorge *2018-02-26 05:49:12*

**Inicial plot**


[c94f4cde92ecabb](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/c94f4cde92ecabb) jorge *2018-02-26 05:46:13*

**no bintrayPublish with citlab**


[e694fe6e5b496dc](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/e694fe6e5b496dc) jorge *2017-11-07 21:22:02*

**type en header**


[b9e3884335c426c](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/b9e3884335c426c) jorge *2017-10-17 20:10:27*

**resto de temas para docu**


[54f9592816f48cf](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/54f9592816f48cf) jorge *2017-10-08 08:11:17*

**resto de temas para docu**


[07f660e30755830](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/07f660e30755830) jorge *2017-10-08 08:03:18*

**improve theme documentation**


[0ab1dd361232138](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/0ab1dd361232138) jorge *2017-09-24 09:49:09*

**improve theme documentation**


[90707b3255a30cf](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/90707b3255a30cf) jorge *2017-09-24 09:43:47*

**set artifactId in maven publications**


[d3920ad97e24e8b](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/d3920ad97e24e8b) jorge *2017-09-24 09:34:18*

**borrar out**


[ce7f9563cce5343](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/ce7f9563cce5343) jorge *2017-09-24 09:19:02*

**themes**


[765a94a5db63c33](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/765a94a5db63c33) jorge *2017-09-24 09:17:30*

**intento de themes**


[c2e30d5c0fd9515](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/c2e30d5c0fd9515) jorge *2017-09-23 19:13:52*

**progress with tooltip**


[9f1e78cb6783276](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/9f1e78cb6783276) jorge *2017-07-15 09:46:14*

**Tooltip en callouts**


[acfdd4b16aab185](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/acfdd4b16aab185) jorge *2017-07-15 09:04:54*

**typos en docu de googlesearch**


[ee5d941726f0b21](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/ee5d941726f0b21) jorge *2017-06-30 18:58:31*

**primer intento de google seach**


[21139c4337b4d31](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/21139c4337b4d31) jorge *2017-06-30 18:49:06*

**a litte doc**


[4857de0de4d73f7](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/4857de0de4d73f7) jorge *2017-06-24 16:18:48*


## 1.6
### No issue

**v1.6 tooltip at callout**


[0222f742372f12c](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/0222f742372f12c) Jorge Aguilera *2017-08-21 09:03:54*


## 1.5.6
### No issue

**v1.5.6**


[2b423e13cdc0a40](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/2b423e13cdc0a40) Jorge Aguilera *2017-08-21 08:02:44*


## v1.5
### No issue

**I forget to add the js**


[e79532c104ce820](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/e79532c104ce820) jorge *2017-06-24 10:51:43*

**only blocks with id**


[d8d509cb8ca5dbe](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/d8d509cb8ca5dbe) jorge *2017-06-24 10:42:01*

**porque lo dice miri**


[6d8498d556679c5](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/6d8498d556679c5) jorge *2017-06-24 10:31:39*

**porque lo dice miri**


[b3c985fad9e1ceb](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/b3c985fad9e1ceb) jorge *2017-06-24 09:36:09*

**porque lo dice miri**


[6291957c5434745](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/6291957c5434745) jorge *2017-06-24 09:05:10*

**porque lo dice miri**


[33e2115044b52d4](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/33e2115044b52d4) jorge *2017-06-24 08:37:36*

**porque lo dice miri**


[540224d6e484b6d](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/540224d6e484b6d) jorge *2017-06-24 08:27:21*

**improve copypaste UI**


[982db4d02b3562c](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/982db4d02b3562c) jorge *2017-06-24 08:16:37*

**improve copypaste UI**


[94c0f9797f3db71](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/94c0f9797f3db71) jorge *2017-06-24 08:07:33*

**improve copypaste UI**


[d2e8f391c80e3ea](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/d2e8f391c80e3ea) jorge *2017-06-24 07:58:33*

**be able to disable copy functionality**


[46a7575b3cf4c04](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/46a7575b3cf4c04) jorge *2017-06-24 07:56:24*

**give some feedback about copypaste action**


[e798284c1fa52cd](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/e798284c1fa52cd) jorge *2017-06-24 07:47:47*

**remove borde on focus**


[a0215db88d94807](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/a0215db88d94807) jorge *2017-06-24 07:38:52*

**add a readme**


[e27ed0f6ae3b4e4](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/e27ed0f6ae3b4e4) jorge *2017-06-24 07:31:39*

**1.5**


[97446f255680cc7](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/97446f255680cc7) Jorge Aguilera *2017-06-24 07:21:25*

**add doc to toc-collapsable**


[beeef262bb434e0](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/beeef262bb434e0) jorge *2017-06-17 09:35:39*


## v1.4
### No issue

**collapsable toc**


[a50190ac89f3923](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/a50190ac89f3923) jorge *2017-06-17 09:01:17*

**some progress with collapsable**


[c6826d2bafbec13](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/c6826d2bafbec13) jorge *2017-06-15 06:02:08*


## v1.3
### No issue

**moved GoogleAnalytics as Docinfo**


[22fd4e015dffa39](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/22fd4e015dffa39) jorge *2017-06-13 21:34:09*

**update documentation**


[f69ffe9a27fccbc](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/f69ffe9a27fccbc) jorge *2017-06-11 12:36:51*


## v1.2
### No issue

**update documentation**


[dd9b5921eba6445](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/dd9b5921eba6445) jorge *2017-06-11 12:10:26*

**disqus inline macro**


[27cabb8e34fccfe](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/27cabb8e34fccfe) jorge *2017-06-11 12:06:07*


## v1.1
### No issue

**double tag script removed**


[ce065c6fc228557](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/ce065c6fc228557) jorge *2017-06-11 11:06:05*

**documentation improved**


[1f98305e83467db](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/1f98305e83467db) jorge *2017-06-11 09:36:34*

**some reorganization**


[e5cf668cb3f9e39](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/e5cf668cb3f9e39) jorge *2017-06-11 09:11:16*

**some reorganization**


[e2a623ac8d367d2](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/e2a623ac8d367d2) jorge *2017-06-11 09:06:59*

**some reorganization**


[31938849fb764c0](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/31938849fb764c0) jorge *2017-06-11 09:05:25*


## v1.0
### No issue

**typo distribution zip path**


[623db75fb42d89a](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/623db75fb42d89a) jorge *2017-06-11 08:33:59*

**deploy to bintray**


[703df7773c4a081](https://gitlab.com/puravida-software/asciidoctor-extensions/commit/703df7773c4a081) jorge *2017-06-11 08:28:29*


