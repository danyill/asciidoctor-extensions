package com.puravida.asciidoctor;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jorge on 10/06/17.
 */
public class ClipboardTests {
    @Test
    public void postProcessorIsApplied() {

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                ":toc: left\n" +
                ":toc-collapsable: \n" +
                "== Hola \n"+
                "caracola \n"+
                "[#bloque1,source]\n"+
                "----\n"+
                "this text\n"+
                "----\n"+
                "[#bloque2,source,groovy]\n"+
                "----\n"+
                "other text\n"+
                "----\n"+
                "[source]\n"+
                "----\n"+
                "this is not a id block\n"+
                "----\n"+
        ""), options);
        //assertThat(converted).contains("data-clipboard-target=\"#bloque1\"");
    }

}
