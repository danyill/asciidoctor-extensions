package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class MultiLanguageTests {

    @Test
    public void postProcessorIsApplied() throws IOException {

        Map<String,Object> attributes = new HashMap<String,Object>();

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        ":toc: left\n" +
                        ":toc-collapsable: \n" +
                        ":multilanguage: gb,es,it \n" +
                        ":multilanguage-toolbar: header \n" +
                        "\n"+
                        "multilanguage::toolbar[] \n"+
                        "\n"+
                        "[role='language-es']\n"+
                        "== Hola \n"+
                        "caracola \n"+
                        "\n"+
                        "[role='language-gb']\n"+
                        "== Hello \n"+
                        "caracol \n"+
                        "\n"+
                        "=== Hi \n"+
                        "caracol \n"+
                        ""), options);
        assertThat(converted).contains("$(document).ready(function() {");
        assertThat(converted).contains("var defaultLang='gb';");
        assertThat(converted).contains("var position='header';");

        File f = new File("build/multilanguage.html");
        System.out.println(f.getAbsolutePath());
        FileOutputStream fout = new FileOutputStream(f);
        fout.write(converted.getBytes());
    }

    //@Test
    public void treeProcessorIsNotAppliedInPdf() throws IOException {

        Map<String,Object> attributes = new HashMap<String,Object>();
        attributes.put("google-analytics-code","UA-aaaaa-aa");

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);
        options.setBackend("pdf");
        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        "ifndef::backend-pdf[]\n" +
                        ":multilanguage: gb,es,it\n" +
                        ":multilanguage-toolbar: toc\n" +
                        ":toc-collapsable:\n" +
                        ":google-analytics-code: UA-687332-11\n" +
                        ":google-search:\n" +
                        "endif::[]\n" +
                        ":toc: left\n" +
                        "== Hola \n"+
                        "caracola \n"+
                        "== Hello \n"+
                        "caracol \n"+
                        "=== Hi \n"+
                        "caracol \n"+
                        ""), options);

        File f = new File("build/multilanguage.pdf");
        System.out.println(f.getAbsolutePath());
        FileOutputStream fout = new FileOutputStream(f);
        fout.write(converted.getBytes());
    }

}
