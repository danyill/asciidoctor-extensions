package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jorge on 30/06/17.
 */
public class GoogleSearchTests {
    @Test
    public void postProcessorIsAppliedInArticle() {
        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("google-search", "");

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        ":doctype: article\n" +
                        ":google-analytics-code: UA-000000-00\n" +
                        "Hola caracola\n\n\n" +
                        "Este es el preface que no dice mucho \n\n" +
                        "==El articulo\n" +
                        "El texto del articulo"), options);
        System.out.println(converted);
        assertThat(converted).contains("\"@type\": \"Article\"");
    }

    @Test
    public void postProcessorIsAppliedInBook() {
        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("google-search", "");

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        ":doctype: book\n" +
                        ":google-analytics-code: UA-000000-00\n" +
                        "Hola caracola"), options);
        System.out.println(converted);
        assertThat(converted).contains("\"@type\": \"Book\"");
    }
}
